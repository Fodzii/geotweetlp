package com.sophiaantipolis.quacheton.geotweetlp;

import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.SearchService;

import java.util.ArrayList;

/**
 * Created by Fodzii on 30/01/2017.
 */

class Tweets {

    private static ArrayList<Tweet> tweets = new ArrayList<>();

    public static ArrayList<Tweet> getTweets()
    {
        return tweets;
    }

    public static void setTweets(ArrayList<Tweet> tweets) {
        tweets = new ArrayList<>();
        tweets = tweets;
    }

    public static void queryTweetsByHashTag(String hashTag, int nbTweets){
        tweets.clear(); //Remise à zéro
        try {
            SearchService service = TwitterCore.getInstance().getApiClient(Twitter.getSessionManager().getActiveSession()).getSearchService();
            service.tweets(hashTag, null, null, null, null, nbTweets, null, null,
                    null, true).enqueue(new Callback<Search>() {

                @Override
                public void success(Result<Search> result) {
                    for (Tweet tweet : result.data.tweets) {
                        tweets.add(tweet);
                        Log.d("Twitter", tweet.text);
                    }
                }

                @Override
                public void failure(TwitterException exception) {
                    Log.e("Twitter", "Veuillez saisir une donnée correcte");
                }

            });
        }
        catch (Exception e){

        }
    }
}
