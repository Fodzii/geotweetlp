package com.sophiaantipolis.quacheton.geotweetlp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.tweetui.ToggleImageButton;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;

public class ConnexionFragment extends Fragment {
    //Tags to send the username and image url to next activity using intent
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PROFILE_IMAGE_URL = "image_url";
    public static String TOKEN;
    public static String SECRET;
    TwitterAuthToken authToken;

    View vue;
    //Twitter Login Button
    TwitterLoginButton twitterLoginButton;

    public ConnexionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vue = inflater.inflate(R.layout.fragment_connexion, container, false);

        //Initializing twitter login button
        twitterLoginButton = (TwitterLoginButton) vue.findViewById(R.id.twitterLogin);

        if (authToken != null){
            twitterLoginButton.setEnabled(false);
            twitterLoginButton.setText("Connecté");
        }
        else{
            Twitter.getSessionManager().clearActiveSession();
        }

        //Adding callback to the button
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                //If login succeeds passing the Calling the login method and passing Result object
                Log.d("TwitterKit", "Login with Twitter sucess");
                login(result);
            }

            @Override
            public void failure(TwitterException exception) {
                //If failure occurs while login handle it here
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });
        return vue;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        twitterLoginButton.onActivityResult(requestCode, resultCode, data);
    }

    //The login function accepting the result object
    public void login(Result<TwitterSession> result) {

        //Creating a twitter session with result's data
        TwitterSession session = result.data;

        //Getting the username from session
        final String username = session.getUserName();


        //Getting the account service of the user logged in
        Call<User> call = Twitter.getApiClient(session).getAccountService()
                .verifyCredentials(true, false);
        call.enqueue(new Callback<User>() {
            @Override
            public void failure(TwitterException e) {
                //If any error occurs handle it here
            }

            @Override
            public void success(Result<User> userResult) {
                //If it succeeds creating a User object from userResult.data
                User user = userResult.data;

                //Getting the profile image url
                String profileImage = user.profileImageUrl.replace("_normal", "");

                twitterLoginButton.setEnabled(false);
                twitterLoginButton.setText("Connecté");
                Toast.makeText(getContext(), "Connexion de " + username + " réussie", Toast.LENGTH_SHORT).show();

                TwitterSession session = Twitter.getSessionManager().getActiveSession();
                authToken = session.getAuthToken();
                TOKEN = authToken.token;
                SECRET = authToken.secret;

            }
        });

    }
}